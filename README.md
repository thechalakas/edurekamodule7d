# map display and location tracking

1. map display
2. asking for permission for location.
3. move map as location changes. 
4. show a pin. 

NOTE : use the debug > Location > Free Way Drive to see the app in action. 

NOTE : stop the location debug on simulator and then zoom to map to the tracked pins. 


**references and links**

no specific references that I can talk about. 

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 