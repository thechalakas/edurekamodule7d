//
//  ViewController.swift
//  EdurekaModule7d
//
//  Created by Jay on 23/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit
//for mapping related things. 
import MapKit


//CLLocationManagerDelegate collects map events.

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate
{
    @IBOutlet var mapView: MKMapView!
    
    //get a location manager
    let locationManager = CLLocationManager()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //collect the events of location collection
        locationManager.delegate = self
        //collection the events of map view
        mapView.delegate = self
        
        //make sure we get permission
        locationManager.requestAlwaysAuthorization()
        
        //start collection location related updates.
        locationManager.startUpdatingLocation()
        
        
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //triggers when permissions is received.
    func locationManager(_ manager: CLLocationManager,
                                  didChangeAuthorization status: CLAuthorizationStatus)
    {
        print("authorization received - " + String(status.rawValue))
    }
    
    //triggers when new update is available
    func locationManager(_ manager: CLLocationManager,
                                  didUpdateLocations locations: [CLLocation])
    {
        print("new location received - " + String(describing: locations.first?.coordinate))
        //get the default pin of the user.
        mapView.showsUserLocation = true
        //move the camera to the coordinat that we just recieved.
        mapView.setCenter((locations.first?.coordinate)!, animated: true)
        
        //create a simple annotation and show it on map
        let setmapPoint = MKPointAnnotation()
        setmapPoint.coordinate = (locations.first?.coordinate)!
        mapView.addAnnotation(setmapPoint)
        
        
        
    }
    
    //if there is some issue is getting locatin updates.
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error)
    {
        print("new location received - Error")
    }
    
    func mapViewWillStartLocatingUser(_ mapView: MKMapView)
    {
        print("map has moved")
    }

}

